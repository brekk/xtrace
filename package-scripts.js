module.exports = {
  scripts: {
    build: `rollup -c rollup.config.js`,
    test: `jest --verbose --coverage`,
    lint: `eslint src/*.js --env jest --fix`,
    doc: `documentation -c documentation.yml readme -s API src/*.js`,
    care: `nps build test lint doc`
  }
}
