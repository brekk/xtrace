import { $ } from "katsu-curry"

import { callWithScopeWhen, callBinaryWithScopeWhen } from "./side-effect"

import { I, always } from "./utils"

// which scope? none
export const callWhen = callWithScopeWhen($, $, I)
// call when? always
export const call = callWithScopeWhen($, always, I)
export const callWithScope = callWithScopeWhen($, always)
// no scope - what === identity
export const callBinaryWhen = callBinaryWithScopeWhen($, $, I)

// no condition - when === always
export const callBinaryWithScope = callBinaryWithScopeWhen($, always)

// no condition, no scope - when === always && what === identity
export const callBinary = callBinaryWithScopeWhen($, always, I)

/**
 * call a side-effect function with a specific context,
 * always return last param
 * @method callWithScope
 * @param {function} effect - a function to call as a side-effect
 * @param {function} what - a lens, e.g. x => x.id
 * @param {*} value - a value
 * @return {*} `value`
 * @example
 * import { callWithScope } from "xtrace"
 * import { prop } from "ramda"
 * const logWithScope = callWithScope(console.log)
 * const getId = prop('id')
 * // ... log as you calculate
 * pipe(
 *   logWithScope( getId, 'the id')
 *   calculate,
 *   logWithScope( x => x.value, 'the value')
 * )
 */
/**
 * call a side-effect function conditionally,
 * always return last param
 * @method callWhen
 * @param {function} effect - a function to call as a side-effect
 * @param {function} when - unary predicate
 * @param {*} value - a value
 * @return {*} `value`
 * @example
 * import { callWhen } from "xtrace"
 * import { prop } from "ramda"
 * const logWhen = callWhen(console.log)
 * const idAbove = above => (_, {id}) => id > above
 * // ... log as you calculate
 * pipe(
 *   logWhen(idAbove(300), 'above three hundred'),
 *   calculate
 * )
 */

/**
 * call a side-effect function,
 * always return last param
 * @method call
 * @alias sideEffect
 * @param {function} effect - a unary function to call as a side-effect
 * @param {function} when - binary predicate
 * @param {*} value - a value
 * @return {*} `value`
 * @example
 * import { call } from "xtrace"
 * import { prop } from "ramda"
 * const log = call(console.log)
 * // ... log as you calculate
 * pipe(
 *   log,
 *   calculate
 * )
 */

/**
 * call a binary side-effect function,
 * with a specific scope,
 * always return the last param
 * @method callBinaryWithScope
 * @alias scopedSideEffect
 * @param {function} effect - a function to call as a side-effect
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinaryWithScope } from "xtrace"
 * import { prop } from "ramda"
 * const logWithScope = callBinaryWithScope(console.log)
 * // ... log as you calculate
 * pipe(
 *   logWithScope(prop('id'), 'the id'),
 *   calculate
 * )
 */

/**
 * call a binary side-effect function,
 * conditionally,
 * always return the last param
 * @method callBinaryWhen
 * @param {function} effect - a function to call as a side-effect
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinaryWhen } from "xtrace"
 * const logWhen = callBinaryWhen(console.log)
 * // ... log as you calculate
 * pipe(
 *   logWhen(({id}) => id === 200, 'the 200 id item'),
 *   calculate
 * )
 */

/**
 * call a binary side-effect function,
 * always return the last param
 * @method callBinary
 * @alias taggedSideEffect
 * @param {function} effect - a function to call as a side-effect
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinary } from "xtrace"
 * const log = callBinary(console.log)
 * // ... log as you calculate
 * pipe(
 *   log('data'),
 *   calculate
 * )
 */
