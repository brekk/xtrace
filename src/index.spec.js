import { callWithScopeWhen, callBinaryWithScopeWhen } from "./index"

test(`callWithScopeWhen - best case`, done => {
  const blah = Math.round(Math.random() * 10e3)
  const say = actual => {
    expect(actual).toEqual(blah)
    done()
  }
  const when = () => true
  const that = x => x
  callWithScopeWhen(say, when, that, blah)
})
test(`callWithScopeWhen - condition false`, done => {
  const blah = Math.round(Math.random() * 10e3)
  const say = done // won't run
  const when = () => false
  const that = x => x
  callWithScopeWhen(say, when, that, blah)
  expect(typeof say).toEqual(`function`)
  setTimeout(() => done(), 100)
})
test(`callWithScopeWhen - scope`, done => {
  const blah = { xxx: Math.round(Math.random() * 10e3) }
  const say = actual => {
    expect(actual).toEqual(blah.xxx)
    done()
  }
  const when = () => true
  const that = x => x.xxx
  callWithScopeWhen(say, when, that, blah)
})

test(`callBinaryWithScopeWhen - best case`, done => {
  const we = `this is a tag`
  const situation = { cool: true, y: `why not?` }
  const say = (tag, actual) => {
    expect(tag).toEqual(we)
    expect(actual).toEqual(situation.y)
    done()
  }
  const when = () => true
  const that = x => x.y
  callBinaryWithScopeWhen(say, when, that, we, situation)
})

test(`callBinaryWithScopeWhen - no case`, done => {
  const we = `this shouldn't have run`
  const situation = { cool: true, y: `why not?` }
  const when = () => false
  const that = x => x.y
  callBinaryWithScopeWhen(done, when, that, we, situation)
  expect(typeof done).toEqual(`function`)
  setTimeout(() => done(), 100)
})
