export {
  call as sideEffect,
  callBinary as taggedSideEffect,
  callBinaryWithScope as scopedSideEffect
} from "./derived"
export { traceWithScope as scopedTrace } from "./log"
/**
 * Now deprecated. See [call](#call) instead.
 * @method sideEffect
 * @deprecated
 * @alias call
 * @param {function} effect - a function to call as a side-effect
 * @param {function} when - binary predicate
 * @param {*} value - a value
 * @return {*} `value`
 * @example
 * import { call as sideEffect } from "xtrace"
 * import { prop } from "ramda"
 * const log = sideEffect(console.log)
 * // ... log as you calculate
 * pipe(
 *   log,
 *   calculate
 * )
 */
/**
 * Now deprecated. See [callBinary](#callBinary) instead.
 * @alias callBinary
 * @deprecated
 * @method taggedSideEffect
 * @param {function} effect - a function to call as a side-effect
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinary as taggedSideEffect } from "xtrace"
 * const log = taggedSideEffect(console.log)
 * // ... log as you calculate
 * pipe(
 *   log('data'),
 *   calculate
 * )
 */

/**
 * Now deprecated. See [callBinaryWithScope](#callBinaryWithScope) instead.
 * @alias callBinaryWithScope
 * @method scopedSideEffect
 * @deprecated
 * @param {function} effect - a function to call as a side-effect
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinaryWithScope as scopedSideEffect } from "xtrace"
 * import { prop } from "ramda"
 * const logWithScope = scopedSideEffect(console.log)
 * // ... log as you calculate
 * pipe(
 *   logWithScope(prop('id'), 'the id'),
 *   calculate
 * )
 */

/**
 * Now deprecated. See [traceWithScope | inspect](#inspect) instead.
 * @alias traceWithScope
 * @method scopedTrace
 * @deprecated
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { inspect } from "xtrace"
 * import { prop } from "ramda"
 * // ... log as you calculate
 * pipe(
 *   inspect(
 *     prop('id'),
 *     'the 200 id item'
 *   ),
 *   calculate
 * )
 */
