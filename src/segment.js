import { curryObjectN } from "katsu-curry"
import { I, always } from "./utils"

/**
 * callBinaryWithScopeWhen but with a curried object style interface.
 * See also [callBinaryWithScopeWhen](#callbinarywithscopewhen).
 * @method segment
 * @param {object} config - configuration object
 * @param {function} config.what - optional lens function, e.g. `x => x.y`
 * @param {function} config.when - optional binary predicate
 * @param {function} config.call - side-effect function
 * @param {*} config.tag - anything
 * @param {*} config.value - anything
 * @return {*} `config.value`
 * @example
 * import {segment} from "xtrace"
 * import {prop} from "ramda"
 * // ... log the item whose id is 300 before calculating
 * pipe(
 *   value => segment({
 *     when: id => id === 300,
 *     what: prop('id'),
 *     call: console.log,
 *     tag: 'the 300 item',
 *     value
 *   })
 *   reduce(calculate, {})
 * )
 */
export const segment = curryObjectN(
  3,
  ({ what = I, when = always, tag, value, effect }) => {
    if (when(tag, what(value))) {
      effect(tag, what(value))
    }
    return value
  }
)

/**
 * traceWithScopeWhen but with a curried object style interface.
 * See also [traceWithScopeWhen](#tracewithscopewhen).
 * @method traceSegment
 * @param {object} config - configuration object
 * @param {function} config.what - lens function, e.g. `x => x.y`
 * @param {function} config.when - binary predicate
 * @param {*} config.tag - anything
 * @param {*} config.value - anything
 * @return {*} `config.value`
 * @example
 * import {segment} from "xtrace"
 * import {prop} from "ramda"
 * // ... log the item whose id is 300 before calculating
 * pipe(
 *   value => traceSegment({
 *     when: id => id === 300,
 *     what: prop('id'),
 *     tag: 'the 300 item',
 *     value
 *   })
 *   reduce(calculate, {})
 * )
 */
export const segmentTrace = segment({ effect: console.log })
