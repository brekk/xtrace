import { segment } from "./segment"

test(`segment`, done => {
  const A = `segment`
  const B = Math.round(Math.random() * 1000)
  const out = segment({
    tag: A,
    value: B,
    effect: (tag, value) => {
      expect(tag).toEqual(A)
      expect(value).toEqual(B)
      done()
    }
  })
  expect(out).toEqual(B)
})

test(`segment - false`, done => {
  const A = `segment`
  const B = Math.round(Math.random() * 1000)
  const out = segment({
    when: () => false,
    tag: A,
    value: B,
    effect: done
  })
  expect(out).toEqual(B)
  setTimeout(() => done(), 1e3)
})
