import { curry } from "katsu-curry"

/**
 * call a side-effect function conditionally with a specific context,
 * always return last param
 * @method callWithScopeWhen
 * @param {function} effect - a function to call as a side-effect
 * @param {function} when - unary predicate
 * @param {function} what - a lens, e.g. x => x.id
 * @param {*} value - a value
 * @return {*} `value`
 * @example
 * import { callWithScopeWhen } from "xtrace"
 * import { prop } from "ramda"
 * const logWhen = callWithScopeWhen(console.log)
 * const getId = prop('id')
 * const idAbove = above => id => id > above
 * // ... log as you calculate
 * pipe(
 *   logWhen(idAbove(300), getId, 'above three hundred'),
 *   calculate
 * )
 */
export const callWithScopeWhen = curry((effect, when, what, value) => {
  const scope = what(value)
  if (when(scope)) effect(scope)
  return value
})

/**
 * call a binary side-effect function,
 * conditionally,
 * with a specific scope,
 * always return the last param
 * @method callBinaryWithScopeWhen
 * @param {function} effect - a function to call as a side-effect
 * @param {function} when - a binary predicate
 * @param {function} what - a lens, e.g. x => x.value
 * @param {*} tag - anything
 * @param {*} value - anything
 * @return {*} `value`
 * @example
 * import { callBinaryWithScopeWhen } from "xtrace"
 * import { prop } from "ramda"
 * const logWithScopeWhen = callBinaryWithScopeWhen(console.log)
 * // ... log as you calculate
 * pipe(
 *   logWithScopeWhen(
 *     id => id === 200,
 *     prop('id'),
 *     'the 200 id item'
 *   ),
 *   calculate
 * )
 */

export const callBinaryWithScopeWhen = curry(
  (effect, when, what, tag, value) => {
    const scope = what(value)
    if (when(tag, scope)) effect(tag, scope)
    return value
  }
)
