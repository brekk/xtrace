'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var katsuCurry = require('katsu-curry');

var callWithScopeWhen = katsuCurry.curry(function (effect, when, what, value) {
  var scope = what(value);
  if (when(scope)) effect(scope);
  return value;
});
var callBinaryWithScopeWhen = katsuCurry.curry(function (effect, when, what, tag, value) {
  var scope = what(value);
  if (when(tag, scope)) effect(tag, scope);
  return value;
});

var always = function always() {
  return true;
};
var I = function I(x) {
  return x;
};

var callWhen = callWithScopeWhen(katsuCurry.$, katsuCurry.$, I);
var call = callWithScopeWhen(katsuCurry.$, always, I);
var callWithScope = callWithScopeWhen(katsuCurry.$, always);
var callBinaryWhen = callBinaryWithScopeWhen(katsuCurry.$, katsuCurry.$, I);
var callBinaryWithScope = callBinaryWithScopeWhen(katsuCurry.$, always);
var callBinary = callBinaryWithScopeWhen(katsuCurry.$, always, I);

var traceWithScopeWhen = callBinaryWithScopeWhen(console.log);
var traceWithScope = traceWithScopeWhen(always);
var inspect = traceWithScope;
var trace = inspect(I);
var traceWhen = callBinaryWithScopeWhen(console.log, katsuCurry.$, I);

var segment = katsuCurry.curryObjectN(3, function (_ref) {
  var _ref$what = _ref.what,
      what = _ref$what === void 0 ? I : _ref$what,
      _ref$when = _ref.when,
      when = _ref$when === void 0 ? always : _ref$when,
      tag = _ref.tag,
      value = _ref.value,
      effect = _ref.effect;
  if (when(tag, what(value))) {
    effect(tag, what(value));
  }
  return value;
});
var segmentTrace = segment({
  effect: console.log
});

exports.$ = katsuCurry.$;
exports.callWithScopeWhen = callWithScopeWhen;
exports.callBinaryWithScopeWhen = callBinaryWithScopeWhen;
exports.callWithScope = callWithScope;
exports.callWhen = callWhen;
exports.call = call;
exports.sideEffect = call;
exports.callBinaryWithScope = callBinaryWithScope;
exports.scopedSideEffect = callBinaryWithScope;
exports.callBinaryWhen = callBinaryWhen;
exports.callBinary = callBinary;
exports.taggedSideEffect = callBinary;
exports.traceWithScopeWhen = traceWithScopeWhen;
exports.traceWhen = traceWhen;
exports.traceWithScope = traceWithScope;
exports.scopedTrace = traceWithScope;
exports.trace = trace;
exports.inspect = inspect;
exports.always = always;
exports.I = I;
exports.segment = segment;
exports.segmentTrace = segmentTrace;
